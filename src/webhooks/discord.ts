import { Webhook } from 'webhook-discord';

export const notify = (diff: string, webhookUrl: string, targetName: string): void => {
    const webhook = new Webhook(webhookUrl);
    let message = `New changes detected to the ${targetName} API:
\`\`\`diff
${diff}
\`\`\``;

    if (message.length > 2000) {
        message = `New changes detected to the ${targetName} API but they are too big to post to discord.`;
    }

    webhook.info('GraphQL Monitor', message);
};